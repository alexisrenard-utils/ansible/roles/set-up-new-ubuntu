# Ansible Role: Set up a fresh new Ubuntu


Project based on Alexis Renard's Ubuntu (18.04) configuration. 
## Usage

* Clone the project and its submodule(s)
```bash
git clone --recurse-submodules <this repo>
```
* Initiate the submodule(s) recursively - the right way
```bash
git submodule update --init --recursive
git submodule foreach --recursive git fetch
git submodule foreach --recursive git checkout master
```
* You're ready to go !

☝️ *git version 2.17.1*

## Variables:

* **`user_name`**:

  * Default : `admin`
  * Description : ``



* **`copy_local_key`**:

  * Default : `false`
  * Description : ``



* **`local_key`**:

  * Default : `"{{ lookup('file', lookup('env','HOME') + '/.ssh/id_rsa.pub') }}"`
  * Description : ``



* **`install_sys_packages`**:

  * Default : `false`
  * Description : ``



* **`sys_packages`**:

  * Default : `[ 'curl', 'vim', 'git', 'ufw', 'openssh-server', 'net-tools', 'htop',  'screen']`
  * Description : ``



* **`additional_sys_packages`**:

  * Default : `[]`
  * Description : ``



* **`install_user_experience_packages`**:

  * Default : `false`
  * Description : ``



* **`user_experience_packages`**:

  * Default : `['zsh', 'tree']`
  * Description : ``



* **`additional_user_experience_packages`**:

  * Default : `[]`
  * Description : ``



* **`install_oh_my_zsh`**:

  * Default : `true`
  * Description : ``



* **`install_graphic_interface_packages`**:

  * Default : `false`
  * Description : ``



* **`graphic_interface_packages_ppa_to_add`**:

  * Default : `ppa:daniruiz/flat-remix`
  * Description : ``



* **`additional_graphic_interface_packages`**:

  * Default : `[]`
  * Description : ``


## Tags:
## License
Project under the
 [MIT](https://tldrlegal.com/license/mit-license)
 License.

## Appendix
### Working with git submodules

* Add a submodule
```bash
git submodule add <repo_url>
```

* Update the submodules
```bash
git submodule update --recursive
git submodule foreach --recursive git fetch
git submodule foreach --recursive git checkout master
```

* Remove a gitsubmodule in a proper way ([check out the mess over there](https://stackoverflow.com/questions/1260748/how-do-i-remove-a-submodule/36593218#36593218))
```bash
# Remove the submodule entry from .git/config
git submodule deinit -f path/to/submodule

# Remove the submodule directory from the superproject's .git/modules directory
rm -rf .git/modules/submodule_name

# Remove the entry in .gitmodules and remove the submodule directory located at path/to/submodule
git rm -f path/to/submodule
```

*git version 2.17.1*



## Author Information
This role  was created by: [alexis_renard](https://renardalexis.com)

👉 https://fr.linkedin.com/in/renardalexis


## Automatically generated
Documentation generated using ansible-autodoc : [https://gitlab.com/alexisrenard-utils/ansible/ansible-autodoc](https://gitlab.com/alexisrenard-utils/ansible/ansible-autodoc)