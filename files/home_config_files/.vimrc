" TODO: see https://github.com/victormours/dotfiles/blob/master/vim/vimrc
"

"""""""""""""""""""""""""""""""""""
"           SETTINGS
"""""""""""""""""""""""""""""""""""

" Change <Leader> key and longer pressed
let mapleader=","
set timeout timeoutlen=1500

set rtp+=/usr/local/lib/python2.7/dist-packages/powerline/bindings/vim/
set t_Co=256

" Display filename, row and column ..
set laststatus=2

" Highlighted search
set hlsearch

" Cursor not on first line
set scrolloff=5

" Keep 'Undo' even after the file is closed and re-opened
set undofile

" Store swap files in fixed location, not current directory.
set backupdir=~/.vim/backup//
set undodir=~/.vim/undo//
set directory=~/.vim/swap//

" Enable mouse support
set mouse=a

" Ctrl+arrows with tmux
" map <esc>[1;5D <C-Left>]
" map <esc>[1;5C <C-Right>]

" Vim Buftabline
"   See: https://github.com/ap/vim-buftabline

"" PG's config 
"" set hidden
"" map F2> :bprevious<CR>
"" map <F3> :bnext<CR>
"" map <F1> <F2>
"" imap <F1> <F2>

"" Own's config
set hidden
nnoremap <C-n> :bnext<CR>
nnoremap <C-q> :bd<CR>

" Paste mode
set pastetoggle=<F5>

"During a search (typically with `/` or `?`), hit Ctrl+L in command mode, and the highlighting disappears
nnoremap <silent> <C-L> :nohlsearch<CR><C-L>



" Terraform conf

" (Optional)Hide Info(Preview) window after completions
autocmd CursorMovedI * if pumvisible() == 0|pclose|endif
autocmd InsertLeave * if pumvisible() == 0|pclose|endif

" (Optional) Enable terraform plan to be include in filter
let g:syntastic_terraform_tffilter_plan = 1

" (Optional) Default: 0, enable(1)/disable(0) plugin'`s keymapping
let g:terraform_completion_keys = 1

" (Optional) Default: 1, enable(1)/disable(0) terraform module registrycompletion
let g:terraform_registry_module_completion = 0"


" Pathogen: plugins loader
"  https://github.com/tpope/vim-pathogen
" execute pathogen#infect()


""""""""""""""""""""""""""""""""""""""""""
" vim-plug: Plugin manager
"  https://github.com/junegunn/vim-plug
""""""""""""""""""""""""""""""""""""""""""

" Download manager if not exist
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

"""
" Plugin list
"   references:
"   - http://xmodulo.com/turn-vim-full-fledged-ide.html
call plug#begin()

" Buftabline
Plug 'ap/vim-buftabline'

" SuperTab: Enhanced Tab completion
Plug 'ervandew/supertab'

" Syntastic: Syntax checking in real-time
Plug 'vim-syntastic/syntastic'
source ~/.vim/config/syntastic

" AutoPairs: Create & indent brackets
Plug 'jiangmiao/auto-pairs'

" NERDcommenter: easy comment lines
"   info: <Leader>c<Space> pour commenter/décommenter
Plug 'scrooloose/nerdcommenter'

" Snipmate: Add code automatically
"  warning: dependencies with other plugins
"  error: conflict with Supertab
Plug 'garbas/vim-snipmate'
Plug 'marcweber/vim-addon-mw-utils'
Plug 'tomtom/tlib_vim'

" NERDtree: file explorer (git compatibility)
Plug 'scrooloose/nerdtree'
Plug 'Xuyuanp/nerdtree-git-plugin'
source ~/.vim/config/nerdtree

" Terraform plugins
Plug 'majutsushi/tagbar'
Plug 'hashivim/vim-terraform'
Plug 'juliosueiras/vim-terraform-completion'

" Dracula: Dark theme
Plug 'dracula/vim', { 'as': 'dracula' }

" Lightline: Add bottom statusbar
Plug 'itchyny/lightline.vim'
source ~/.vim/config/lightline

" Lightline-bufferline: bufferline functionality for the lightline vim plugin
Plug 'mengelbrecht/lightline-bufferline'

" List ends here. Plugins become visible to Vim after this call.
call plug#end()
